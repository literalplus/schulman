﻿/*************************************************************\\
 * File: Stundenplan.xaml.cs
 * Schulman v(see SchulMan.Version in File App.xaml.cs)
 * by _xxyy(Philipp N.)
 * xxyy98+schulman@gmail.com
 * http://xxyy.bplaced.net/
 * 
 * For full source code and updates, please check
 * <http://bit.ly/_schulman>
 *   
 * This program is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU General
 *   Public License as published by the Free Software
 *   Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *   
 * This program is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied
 *   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *   PURPOSE.  See the GNU General Public License for more
 *   details.
 *   
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
\\*************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using xytools;
using System.IO;

namespace schulman
{

    public static class StuPlaColors{
        public static Dictionary<string, SolidColorBrush> colors = new Dictionary<string, SolidColorBrush>();
        public static Dictionary<string, SolidColorBrush> textcolors = new Dictionary<string, SolidColorBrush>();
        public static void refreshcolors()
        {
            if (colors.Count == 0)
            {
                colors.Add("E", Brushes.Orange);
                colors.Add("ME", Brushes.Silver);
                colors.Add("CH", Brushes.Gold);
                colors.Add("PH", Brushes.SkyBlue);
                colors.Add("D", Brushes.Blue);
                colors.Add("SPA", Brushes.Red);
                colors.Add("", Brushes.LightGray);
            }
        }
    }
    /// <summary>
    /// Interaktionslogik für Stundenplan.xaml
    /// </summary>
    public partial class Stundenplan : Page
    {
        public Label[][] labels;
        //public Dictionary<string,SolidColorBrush> colors = StuPlaColors.colors;
        SolidColorBrush[] colors = new SolidColorBrush[19];
        public Stundenplan()
        {
            SchulMan.Stundenplan = this;
            InitializeComponent();
            //refreshcolors();
            //loadmonday();
            labels = new Label[][]{
            new Label[8]{lblMoStd1,lblMoStd2,lblMoStd3,lblMoStd4,lblMoStd5,lblMoStd6,lblMoStd8,lblMoStd9},//monday
            new Label[8]{lblTueStd1,lblTueStd2,lblTueStd3,lblTueStd4,lblTueStd5,lblTueStd6,lblTueStd8,lblTueStd9},//tuesday
            new Label[8]{lblWedStd1,lblWedStd2,lblWedStd3,lblWedStd4,lblWedStd5,lblWedStd6,lblWedStd8,lblWedStd9},//wednesday
            new Label[8]{lblThuStd1,lblThuStd2,lblThuStd3,lblThuStd4,lblThuStd5,lblThuStd6,lblThuStd8,lblThuStd9},//thursady
            new Label[8]{lblFrStd1,lblFrStd2,lblFrStd3,lblFrStd4,lblFrStd5,lblFrStd6,lblFrStd8,lblFrStd9}//friday
        };
            LoadLessonsIfExists();
        }

        public bool LoadLessonsIfExists(bool force=false)
        {
            try
            {
                Lesson[][] lessons = Lessons.LoadLessonsFromConfig(force);
                int j = 0;
                foreach (Lesson[] day in lessons)
                {
                    int i = 0;
                    foreach (Lesson lesson in day)
                    {
                        D.W(j + lesson.Key + i, "j,Lesson.Key,i",CodeArea.LoadLessons);
                        labels[j][i].Content = lesson.Key;
                        labels[j][i].Background = lesson.Background;
                        labels[j][i].Foreground = lesson.Foreground;
                        i++;
                    }
                    j++;
                }
            }
            catch (FileNotFoundException)
            {
                D.W("Config file doesn't exist!");
                gStuPla.Visibility = Visibility.Hidden;
                lblConfigure.Visibility = Visibility.Visible;
                return false;
            }
            catch (Exception e)
            {
                D.W("##################");
                D.W("Eine Ausnahme! (LoadLessonsIfExists)");
                D.W(e.Message);
            }
            return true;
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            //loadmonday();
        }

        private void Hyperlink_Click_1(object sender, RoutedEventArgs e)
        {
            Window1 stuplaset = new Window1();
            stuplaset.Show();
        }

        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            LoadLessonsIfExists();
        }

        private void btnStuPlaSet_Click(object sender, RoutedEventArgs e)
        {
            Window1 stuplaset = new Window1();
            stuplaset.Show();
        }
    }
}
