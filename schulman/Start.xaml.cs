﻿/*************************************************************\\
 * File: Start.xaml.cs
 * Schulman v(see SchulMan.Version in File App.xaml.cs)
 * by _xxyy(Philipp N.)
 * xxyy98+schulman@gmail.com
 * http://xxyy.bplaced.net/
 * 
 * For full source code and updates, please check
 * <http://bit.ly/_schulman>
 *   
 * This program is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU General
 *   Public License as published by the Free Software
 *   Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *   
 * This program is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied
 *   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *   PURPOSE.  See the GNU General Public License for more
 *   details.
 *   
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
\\*************************************************************/
using System;
using System.Windows.Controls;
using System.Windows.Threading;
using xytools;

namespace schulman
{
    /// <summary>
    /// Interaktionslogik für Start.xaml
    /// </summary>
    public partial class Start : Page
    {
        public DateTime[] LessonEndings;
        public Start()
        {
            SchulMan.Start = this;
            InitializeComponent();
                this.txtbTime.Text = DateTime.Now.ToString("HH:mm");
                DispatcherTimer timer = new DispatcherTimer(new TimeSpan(0, 0, 15), DispatcherPriority.Normal, delegate
                {
                    var temp = DateTime.Now.ToString("HH:mm");
                    this.txtbTime.Text = temp;
                    //D.W("Time updated", temp);
                }, this.Dispatcher);
        }
    }
}