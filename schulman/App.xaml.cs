﻿/*************************************************************\\
 * File: App.xaml.cs
 * Schulman v(see SchulMan.Version in File App.xaml.cs)
 * by _xxyy(Philipp N.)
 * xxyy98+schulman@gmail.com
 * http://xxyy.bplaced.net/
 * 
 * For full source code and updates, please check
 * <http://bit.ly/_schulman>
 *   
 * This program is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU General
 *   Public License as published by the Free Software
 *   Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *   
 * This program is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied
 *   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *   PURPOSE.  See the GNU General Public License for more
 *   details.
 *   
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
\\*************************************************************/
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.IO;
using xytools;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Specialized;   

namespace schulman
{
    /// <summary>
    /// Interaktionslogik für "App.xaml"
    /// </summary>
    public partial class App : Application
    {
        public Task splashTask;
        public Thread splashThread;
        public CancellationTokenSource cts;
        public startscreen splashscreen;
        public delegate void delSplashPleaseEnd();
        protected override void OnStartup(StartupEventArgs e)
        {
            
            StuPlaColors.refreshcolors();
            D.W("#################################");
            D.W("# schulman v"+SchulMan.Version+"");
            D.W("# Copyright (C) 2012 xxyy");
            D.W("# http://xxyy.bplaced.net/");
            D.W("# DEBUG SESSION");
            D.W("#################################");
            /*if ((byte)SchulMan.VersionDevState < 6 && SchulMan.VersionDevState != DevState.PublicAlpha)
            {
                D.W("**DO NOT DISTRIBUTE**");
            }*/
            try
            {
                this.cts = new CancellationTokenSource();
                //CancellationToken token = cts.Token;

                D.W("showing splash..");
                //this.splashTask = Task.Factory.StartNew(this.AsyncSplash,cts.Token);
                this.splashThread = new Thread(new ParameterizedThreadStart(this.AsyncSplash));
                this.splashThread.SetApartmentState(ApartmentState.STA);
                this.splashThread.Start(this.cts);
                D.W("1st thread again");
                string dirName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData).ToString(), @".xxyy\schulman\");
                D.W(dirName, "Schulman Main Directory");
                string addonDirName = Path.Combine(dirName, @"addons\");
                string logDirName = Path.Combine(dirName, @"logs\");
                SchulMan.MainDirectory = dirName;
                SchulMan.AddonDirectory = addonDirName;
                SchulMan.LogDirectory = logDirName;
                if (!Directory.Exists(dirName))
                {
                    Directory.CreateDirectory(dirName);
                    D.W("Main Directory didn't exist. Created!");
                }
                if (!Directory.Exists(addonDirName))
                {
                    D.W(@"Creating Add-On Directory (\addons\)");
                    Directory.CreateDirectory(addonDirName);
                }
                if (!Directory.Exists(logDirName))
                {
                    D.W(@"Creating Log Directory (\logs\)");
                    Directory.CreateDirectory(logDirName);
                }
            }
            catch (Exception)
            {

            }
            finally
            {
                //Thread.Sleep(500);//Don't want the Splash to be ignored :P
            }
            base.OnStartup(e);
        }

        /*private void ShowSplashAsync()
        {
            D.W("*Splash thread here. Showing splash now. over!");
            this.splashscreen = new startscreen();
            this.splashscreen.ShowDialog();
        }*/

        [STAThread]
        private void AsyncSplash(object arg)
        {
            try
            {
                if (!(arg is CancellationTokenSource)) Thread.CurrentThread.Abort();
                CancellationTokenSource cts = (CancellationTokenSource)arg;
                D.W("*2nd Thread: Showing Splash!");
                this.splashscreen = new startscreen(cts.Token);
                this.splashscreen.Show();
                
                /*this.splashscreen.Closing += (sender3, e3) =>
                {
                    D.W("2nd thread: fading out...");
                    //this.splashscreen.FadeOut();
                };*/

                this.splashscreen.Closed += (sender2, e2) =>
                {
                    D.W("2nd Thread: Shutting down splash!");
                    this.splashscreen.Dispatcher.InvokeShutdown();
                };
                System.Windows.Threading.Dispatcher.Run();
            }
            catch (Exception e)
            {
                D.W("Exception im 2nd Thread: " + e.Message);
            }
        }
        private void Application_LoadCompleted_1(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            Thread.Sleep(500);
            D.W("1st Thread: Closing Splash!");
            //this.splashscreen.FadeOut();
            //this.splashThread.Abort();
            //this.splashscreen.Close();
            try
            {
                this.cts.Cancel();
            }
            catch (Exception ex)
            {
                D.W("Exception beim Abschalten des Splashs!" + ex.Message);
            }
            //if(SchulMan.mainWin != null) SchulMan.mainWin.Opacity = 100;
            SchulMan.splashAlreadyGone = true;
        }
        public void SendStatus()
        {
            this.splashscreen.Close();
        }
    }
    public enum DevState : byte{
        Pre_Alpha=1,
        Alpha=2,
        PublicAlpha=3,//outdated
        ClosedBeta=4,//notanymore
        Beta=6,
        Pre_Release=9,
        Release=10,
        Unknown=0
    }
    public static class SchulMan{
        public const string Version = "0.1a Bugfix Update #1";
        public const double StandardizedVersion = 0.1013;//Major.minor+fix#(3 digits) ex.: 1.2.23 => 1.2023
        public const DevState VersionDevState = DevState.Alpha;

        public static bool LoggingEnabled = true;
        public static bool LogToFile = false;//NYI
        public static bool LogToConsole = true;
        public static bool LogLessonLoading = false;

        public static string MainDirectory;
        public static string AddonDirectory;
        public static string LogDirectory;
        public static MainWindow mainWin;
        public static Notizen Notizen;
        public static Fahrplan Fahrplan;
        public static Stundenplan Stundenplan;
        public static Start Start;
        public static Window1 stuplaset;
        public static devenv devenv;
        public static startscreen splash;
        public static LessonTypeSettings lessonTypeSet;
        public static bool splashAlreadyGone = false;
    }
}
