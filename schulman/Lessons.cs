﻿/*************************************************************\\
 * File: Lessons.cs
 * Schulman v(see SchulMan.Version in File App.xaml.cs)
 * by _xxyy(Philipp N.)
 * xxyy98+schulman@gmail.com
 * http://xxyy.bplaced.net/
 * 
 * For full source code and updates, please check
 * <http://bit.ly/_schulman>
 *   
 * This program is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU General
 *   Public License as published by the Free Software
 *   Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *   
 * This program is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied
 *   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *   PURPOSE.  See the GNU General Public License for more
 *   details.
 *   
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
\\*************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using xytools;

namespace schulman
{
    public enum LessonType
    {
        E,
        ME,
        CH,
        PH,
        D,
        SPA,
        None
    }

    public class Lesson
    {
        public Brush Background;
        public Brush Foreground = Brushes.Black;
        public string Key;
        

        public Lesson(string Key, Brush Background, Brush Foreground)
            : this(Key, Background)
        {
            this.Foreground = Foreground;
        }

        public Lesson(string Key, Brush Background)
        {
            //if (Background == null) this.Background = Brushes.LightGray;
            this.Background = Background;
            this.Key = Key;
        }

        public void ApplyToLbl(Label lbl)
        {
            lbl.Background = Background;
            lbl.Foreground = Foreground;
            lbl.Content = Key;
        }

        public override string ToString()
        {
            return this.Key;
        }
    }

    public class Lessons
    {
        public static Lesson[][] LessonCache;
        public static bool IsInitialized = false;
        public static Dictionary<string, Lesson> LessonTypes = new Dictionary<string, Lesson>();
        private static int BackgroundCount=0;
        public static Brush[] BackgroundBrushes = new Brush[]{
            Brushes.Silver,Brushes.Gold,Brushes.Wheat,Brushes.White,Brushes.SkyBlue,Brushes.Aquamarine,Brushes.Chocolate,Brushes.Green,Brushes.CornflowerBlue,
            Brushes.DarkGoldenrod,Brushes.Orange,Brushes.Blue,Brushes.Red,Brushes.Yellow,Brushes.Transparent
        };
        public static string[] PreDefLessonTypes = new string[]{
            "ME","CH","GWK","GPB","PH","BSP","REL","BIUK","INF","BE","E","D","SPA","M",""
        };

        /// <summary>
        /// returns a Lesson object located at day, on postion lessonnum
        /// </summary>
        /// <example>GetSingleLesson(DayOfWeek.Monday,2)</example>
        /// <param name="day">What DayOfWeek the Lesson is located at</param>
        /// <param name="lessonnum">What postion in day the lesson is located at. Zero-based!</param>
        /// <returns>The Lesson object</returns>
        public static Lesson GetSingleLesson(DayOfWeek day, uint lessonnum)
        {
            
            return new Lesson("", Brushes.Gray);
        }

        public static Brush GetNextBckgrd()
        {
            return BackgroundBrushes[BackgroundCount++];
        }

        /// <summary>
        /// converts brush[] to string[]
        /// </summary>
        /// <param name="brushes">brush[] to convert</param>
        /// <returns>string[] output</returns>
        public static string[] FromBrushAtoStringA(Brush[] brushes)
        {
            string[] output = new string[brushes.Length];
            byte i = 0;
            try
            {
                foreach (Brush item in brushes)
                {
                    D.W(i, "i");
                    output[i] = item.ToString();
                    i++;
                }
            }
            catch (Exception) 
            {
                D.W("Exceptión! FromBrushAToStringA()");
            }
            return output;
        }

        public static void init()
        {
            try
            {
                LessonTypes = new Dictionary<string, Lesson>();
                string[] fileLines = GetFileLines("lessons.cfg.txt", 2);
                //D.W(fileLines.Length, "filelines lenght");
                D.W(xy_str.listArrayToString(fileLines), "fileLines (Lessontypes)");
                //exception in next ln!
                string TempFileLine1 = fileLines[0]; string TempFileLine2 = fileLines[1];
                if (fileLines == null || fileLines.Length < 2 || fileLines[0] == "" || fileLines[1] == ""||fileLines[0]==null||fileLines[1]==null) throw new FileFormatException("File is empty!");
                D.W("About to call LoadLessonTypesFromLines()");
                LoadLessonTypesFromLines(fileLines);
                D.W("**LessonTypes seem to be loaded correctly :)");
            }
            catch (FileNotFoundException e)
            {
                CreateLessonTypeCfgFile(e);
            }
            catch (FileFormatException e)
            {
                CreateLessonTypeCfgFile(e);
            }
            catch (ArgumentException e)
            {
                CreateLessonTypeCfgFile(e);
            }
            catch (NullReferenceException e)
            {
                CreateLessonTypeCfgFile(e);
            }
            catch (Exception e)
            {
                D.W("Lessons.init(): Exception! " + e.Message + e.Source);
            }
            IsInitialized = true;
        }

        private static void CreateLessonTypeCfgFile(Exception e)
        {
            D.W("Lesson.init(): Index out of Range:"+e.Message+"; Creating...");
            string NewFilePath = Path.Combine(SchulMan.MainDirectory, "lessons.cfg.txt");
            D.W(NewFilePath, "NewFilePath");
            StreamWriter sw = new StreamWriter(NewFilePath, false, Encoding.UTF8);
           // string[] TempArr=xy_str.WriteAndPass(new string[]{});
            sw.Write("{0}\n{1}", xy_str.WriteAndPass<string>(xy_str.implode(PreDefLessonTypes)), xy_str.implode(FromBrushAtoStringA(BackgroundBrushes)));
            sw.Flush();
            sw.Close();
            D.W("Written!");
            foreach (string Key in PreDefLessonTypes)
            {
                D.W(Key, "Key", CodeArea.LoadLessons);
                if (Key == "D") LessonTypes.Add(Key, new Lesson(Key, GetNextBckgrd(), Brushes.White));
                else LessonTypes.Add(Key, new Lesson(Key, GetNextBckgrd()));
            }
        }

        private static void LoadLessonTypesFromLines(string[] fileLines)
        {
            
            try
            {
                //int i = 1;
                string[] lessonNames;
                //D.W("AA");
                lessonNames = xy_str.explode(fileLines[0]);
                int j = 0;
                string[] BackgrdNames = xy_str.explode(fileLines[1]);
                D.W(lessonNames.Length, "lessonsNames.Lenght");
                foreach (string Name in BackgrdNames)
                {
                    //lessonBckgrds[j++] = (Brush)ColorConverter.ConvertFromString(Name);
                    if (lessonNames[j] == null) break;
                    //D.W(j, "j before");
                    D.W("Loading lessontype(Col): "+lessonNames[j]+"+"+Name);
                    Color TempCol = (Color)ColorConverter.ConvertFromString(Name);
                    LessonTypes.Add(lessonNames[j], new Lesson(lessonNames[j], new SolidColorBrush(TempCol)));
                    j++;
                    //D.W(j, "j after");
                }
            }
            catch (IndexOutOfRangeException)
            {
                D.W("LoadLessonsTypes(): IndexOutOfRangeException! <3");
            }
            catch (Exception e)
            {
                D.W("LoadLessonTypesFromLines(): Exception!");
                throw e;
            }
        }

        public static void InitIfNotAlready()
        {
            if (!IsInitialized) init();
        }

        /// <summary>
        /// Loads lessons form config created by SaveLessonsToConfig().
        /// </summary>
        /// <returns>Loaded Lesson[][]</returns>
        /// <exception cref="System.IO.FileNotFoundException" />
        public static Lesson[][] LoadLessonsFromConfig(bool forceLoading=false)
        {
            InitIfNotAlready();
            D.W("#########################", CodeArea.LoadLessons);
            D.W("Lessons.LoadLessonsFromConfig()");
            Lesson[][] lessons = new Lesson[][]{
                new Lesson[8],//monday
                new Lesson[8],//tuesday
                new Lesson[8],//wednesday
                new Lesson[8],//thursday
                new Lesson[8]//friday
            };

            // string path = Environment.SpecialFolder.UserProfile.ToString();
            try
            {
                    if (forceLoading == true || LessonCache == null || LessonCache.Length < 5)
                    {
                        string[] days = Lessons.GetFileLines();

                        lessons = GetLessonsFromLines(days); //code moved to GetLessonsFromLines() 
                        LessonCache = lessons;
                    }
                    else
                    {
                        lessons = LessonCache;
                    }
                
            }
            catch (FileNotFoundException ex)
            {
                throw ex;
            }
            catch (Exception e)
            {
                D.W("Unknown Exception in Lessons.LoadLessonsFromConfig: " + e.Message);
                MessageBox.Show("Unbekannter Fehler");
            }
            D.W("End Lessons.LoadLessonsFromConfig();", CodeArea.LoadLessons);
            D.W("####################################", CodeArea.LoadLessons);
            return lessons;
        }

        /// <summary>
        /// Saves Lessons to config.
        /// !Make sure to catch Exceptions, not catched by method to pass data to devenv.
        /// </summary>
        /// <param name="data">TextBox[]-Array (5 days, 8 lessons/day)</param>
        /// <param name="prob">Optional ProgressBar to display progress</param>
        public static void SaveLessonsToConfig(TextBox[][] data, ProgressBar prob)
        {
            D.W("Saving Lessons to config!");
            InitIfNotAlready();
            string[] TempSaves = new string[] { "", "", "", "", "" };
            int i = 0;
            foreach (TextBox[] day in data)
            {
                foreach (TextBox lesson in day)
                {
                    TempSaves[i] += lesson.Text + ",";
                }
                i++;
                SetBarIfExists(prob, i * 20);
            }
            string dirName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData).ToString(), @".xxyy\schulman\");
            D.W(dirName, "Dir");
            if (!Directory.Exists(dirName))
            {
                Directory.CreateDirectory(dirName);
                D.W("Dir (" + dirName + ") doesn't exist. Created!");
            }
            File.WriteAllLines(Path.Combine(dirName, "stupla.cfg.txt"), TempSaves, Encoding.UTF8);
        }

        /// <summary>
        /// private method to fetch lines from config file.
        /// </summary>
        /// <param name="file">What file in MainDir should I read?</param>
        /// <param name="expectedLineNr">How many lines do you expect, dear Sir?</param>
        /// <returns>Lines of the config file.</returns>
        private static string[] GetFileLines(string file="stupla.cfg.txt",int expectedLineNr = 5)
        {
            string dirName = SchulMan.MainDirectory;
            StreamReader sr = File.OpenText(Path.Combine(dirName, file));
            string[] output = new string[expectedLineNr];
            for (int i = 0; i < expectedLineNr; i++)
            {
                output[i] = sr.ReadLine();
                //D.W(i, "GetFileLines: i");
            }
            //return File.ReadAllLines(, Encoding.UTF8);
            sr.Close();
            return output;
        }
        private static Lesson[][] GetLessonsFromLines(string[] lines)
        {
            Lesson[][] lessons = new Lesson[][]{
                new Lesson[8],//monday
                new Lesson[8],//tuesday
                new Lesson[8],//wednesday
                new Lesson[8],//thursday
                new Lesson[8]//friday
            };
            int i = 0;
            int j = 0;

            foreach (string day in lines)
            {
                //List<Lesson> lessons = new List<Lesson>();
                string TempName = "";
                j = 0;
                foreach (char ch in day)
                {
                    if (ch != ',')
                    {
                        TempName += ch;
                        D.W(ch.ToString(), "Found char != ,", CodeArea.LoadLessons);
                    }
                    else
                    {
                        //not using xy_str.explode() because of getting lessons -> double code. Mybe will be implementing delegate in later version.
                        D.W("\"" + TempName + "\"", "Probably found end of Lesson; String so far", CodeArea.LoadLessons);
                        Lesson TempLess;

                        //LessonTypes.TryGetValue(TempName, out TempLess);
                        if (LessonTypes.ContainsKey(TempName))
                        {
                            TempLess = LessonTypes[TempName];
                            D.W("Lesson known!", CodeArea.LoadLessons);
                        }
                        else
                        {
                            TempLess = new Lesson(TempName, Brushes.LightGray);
                            D.W("Unknown Lesson.", CodeArea.LoadLessons);
                        }
                        lessons[i][j] = TempLess;
                        TempName = "";
                        j++;
                        D.W(j.ToString(), "j", CodeArea.LoadLessons);
                    }
                }//foreach day
                i++;
                D.W(i.ToString(), "i", CodeArea.LoadLessons);
            }//foreach days
            return lessons;
        }

        /// <summary>
        /// Sets Progressbar prob to value if it is a valid ProgressBar object.
        /// </summary>
        /// <param name="prob">object to apply value to if it is ProgressBar. Also accepts null.</param>
        /// <param name="value">The value to apply.</param>
        private static bool SetBarIfExists(object prob, double value)
        {
            InitIfNotAlready();
            if (prob is ProgressBar)
            {
                ((ProgressBar)prob).Value = value;
                return true;
            }
            else return false;
        }
    }
}