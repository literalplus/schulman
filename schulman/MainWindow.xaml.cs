﻿/*************************************************************\\
 * File: MainWindow.xaml.cs
 * Schulman v(see SchulMan.Version in File App.xaml.cs)
 * by _xxyy(Philipp N.)
 * xxyy98+schulman@gmail.com
 * http://xxyy.bplaced.net/
 * 
 * For full source code and updates, please check
 * <http://bit.ly/_schulman>
 *   
 * This program is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU General
 *   Public License as published by the Free Software
 *   Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *   
 * This program is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied
 *   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *   PURPOSE.  See the GNU General Public License for more
 *   details.
 *   
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
\\*************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using xytools;
using _Hotkey;
using System.Windows.Interop;
using System.Runtime.InteropServices;
using System.ComponentModel;
using System.IO;

namespace schulman
{
    public enum Tabs{
        Start,
        Notizen,
        Stundenplan,
        Fahrplan
    }
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private HwndSource hWndSource;
        private short atom;
        public MainWindow()
        {
            SchulMan.mainWin = this;
            InitializeComponent();
            //if (SchulMan.splashAlreadyGone) this.Opacity = 100;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            WindowInteropHelper wih = new WindowInteropHelper(this);
            hWndSource = HwndSource.FromHwnd(wih.Handle);
            hWndSource.AddHook(MainWindowProc);

            // create an atom for registering the hotkey
            atom = Win32.GlobalAddAtom("_Hotkey");

            if (atom == 0)
            {
                throw new Win32Exception(Marshal.GetLastWin32Error());
            }


            // register the Win+Shift+D hotkey
            if (!Win32.RegisterHotKey(wih.Handle, atom, Win32.MOD_WIN | Win32.MOD_SHIFT, Convert.ToUInt32('D')))
            {
                throw new Win32Exception(Marshal.GetLastWin32Error());
            }
            //xytools.AeroTools.RefreshAero(this);
            this.Title = "SchulMan v" + SchulMan.Version;
            setTab(Tabs.Start);
            setTab(Tabs.Notizen);
            setTab(Tabs.Fahrplan);
            setTab(Tabs.Stundenplan);
        }

        private IntPtr MainWindowProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            switch (msg)
            {
                case Win32.WM_HOTKEY:
                  //  D.W("AA :)");
                    devenv de = new devenv();
                    de.Show();
                    handled = true;
                    break;
            }

            return IntPtr.Zero;
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            xytools.AeroTools.RefreshAero(this);
        }

        private void Window_GotFocus(object sender, RoutedEventArgs e)
        {
            xytools.AeroTools.RefreshAero(this);
        }

        private void Window_LayoutUpdated(object sender, EventArgs e)
        {
            xytools.AeroTools.RefreshAero(this);
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            xytools.AeroTools.RefreshAero(this);
        }

        private void Window_StateChanged(object sender, EventArgs e)
        {
            xytools.AeroTools.RefreshAero(this);
        }
        public void setTab(Tabs tab){
            //string name;
            switch(tab){//multiple frames to keep loaded (?)
                case Tabs.Start:
                    setVisibilities(Visibility.Visible, Visibility.Hidden, Visibility.Hidden, Visibility.Hidden);
                    //name = "Start.xaml";
                    //frMain.Background = Brushes.Transparent;
                    break;
                case Tabs.Notizen:
                    setVisibilities(Visibility.Hidden, Visibility.Visible, Visibility.Hidden, Visibility.Hidden);
                    //name = "Notizen.xaml";
                    //frMain.Background = Brushes.AliceBlue;
                    break;
                case Tabs.Stundenplan:
                    setVisibilities(Visibility.Hidden, Visibility.Hidden, Visibility.Visible, Visibility.Hidden);
                    //name = "Stundenplan.xaml";
                    //frMain.Background = Brushes.AliceBlue;
                    break;
                case Tabs.Fahrplan:
                    setVisibilities(Visibility.Hidden, Visibility.Hidden, Visibility.Hidden, Visibility.Visible);
                    //name = "Fahrplan.xaml";
                    //frMain.Background = Brushes.AliceBlue;
                    break;
                default:
                    setVisibilities(Visibility.Visible, Visibility.Hidden, Visibility.Hidden, Visibility.Hidden);
                    //name = "Start.xaml";
                    //frMain.Background = Brushes.Transparent;
                    break;
            }
            //frMain.Source = new Uri(name,UriKind.Relative);
        }

        private void setVisibilities(Visibility start, Visibility notizen, Visibility stupla, Visibility fahrplan)
        {
            frStart.Visibility = start;
            frNotizen.Visibility = notizen;
            frStundenplan.Visibility = stupla;
            frFahrplan.Visibility = fahrplan;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            setTab(Tabs.Start);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            setTab(Tabs.Notizen);
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            setTab(Tabs.Stundenplan);
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            setTab(Tabs.Fahrplan);
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            SchulMan.Stundenplan.LoadLessonsIfExists(true);
        }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            Window1 stuplaset = new Window1();
            stuplaset.Show();
        }

        private void MenuItem_Click_3(object sender, RoutedEventArgs e)
        {
            MessageBoxResult mbr = MessageBox.Show("Are you sure that you want to do this? All custom LessonTypes will be lost FOREVER (a long time!)","Delete",MessageBoxButton.OKCancel);
            if (mbr == MessageBoxResult.Cancel) return;
            File.Delete(System.IO.Path.Combine(SchulMan.MainDirectory, "lessons.cfg.txt"));
        }

        private void MenuItem_Click_4(object sender, RoutedEventArgs e)
        {
            ReloadStuPla();
        }
        private void MenuItem_Click_5(object sender, RoutedEventArgs e)
        {
            Lessons.init();
            //SchulMan.Stundenplan.LoadLessonsIfExists();
        }
        public void ReloadStuPla()
        {
            Lessons.init();
            SchulMan.Stundenplan.LoadLessonsIfExists(true);
        }
    }
}
