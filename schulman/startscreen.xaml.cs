﻿/*************************************************************\\
 * File: startscreen.xaml.cs
 * Schulman v(see SchulMan.Version in File App.xaml.cs)
 * by _xxyy(Philipp N.)
 * xxyy98+schulman@gmail.com
 * http://xxyy.bplaced.net/
 * 
 * For full source code and updates, please check
 * <http://bit.ly/_schulman>
 *   
 * This program is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU General
 *   Public License as published by the Free Software
 *   Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *   
 * This program is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied
 *   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *   PURPOSE.  See the GNU General Public License for more
 *   details.
 *   
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
\\*************************************************************/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using xytools;

namespace schulman
{
    /// <summary>
    /// Interaktionslogik für startscreen.xaml
    /// </summary>
    public partial class startscreen : Window
    {
        //private Storyboard FadeOutStoBoa;

        public startscreen(CancellationToken token)
        {
            InitializeComponent();
            DispatcherTimer timer = new DispatcherTimer(new TimeSpan(0, 0, 0, 0, 250), DispatcherPriority.Normal, delegate
            {
                if (token.IsCancellationRequested)
                {
                    //Thread.BeginCriticalRegion();
                    this.Topmost = true;
                    this.Topmost = false;
                    D.W("Cancellation in 2nd Thread requested!");
                    //Thread.Sleep(3000);
                    //Thread.EndCriticalRegion();
                    for (byte i = 0; i < 30; i++)
                    {
                        this.Topmost = true;
                        this.Topmost = false;
                        Thread.Sleep(125);
                        this.Opacity = this.Opacity - 2;
                    }
                    this.FadeOut();
                    this.Close();
                }
                else
                {
                    this.Topmost = true;
                    this.Topmost = false;
                    D.W("Saved topmost position");
                }
            }, this.Dispatcher);
        }

        private void TextBlock_MouseUp_1(object sender, MouseButtonEventArgs e)
        {
            //MessageBox.Show("minimizing...wait a sec! wtf?");
            this.WindowState = WindowState.Minimized;
            this.ShowInTaskbar = true;
        }

        private void TextBlock_MouseUp_2(object sender, MouseButtonEventArgs e)
        {
            //MessageBox.Show("closing..");
            this.Close();
            Application.Current.Shutdown();
        }

        private void spaGPLUrl_MouseUp(object sender, MouseButtonEventArgs e)
        {
            Process.Start("http://www.gnu.org/licenses/");
        }

        private void spaWebUrl_MouseUp(object sender, MouseButtonEventArgs e)
        {
            Process.Start("http://bit.ly/_xy");
        }

        public void FadeOut()
        {
            /*D.W("In FadeOut()","2nd Thread");
            D.W(this.Opacity, "Opacity");
            for (double i = rctBackgrd.Opacity; i > 0; i = i - 0.1)
            {
                
                D.W(i, "i");
                wdwStartScreen.Opacity = i;
                D.W(this.Opacity, "this.Opacity");
                Thread.Sleep(500);
            }*/
            //rctBackgrd.Focus();
            this.Opacity = 50;
            Thread.Sleep(2100);
        }

        private void rctBackgrd_GotFocus(object sender, RoutedEventArgs e)
        {
            D.W("rct got focus!");
        }
    }
}
