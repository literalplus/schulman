﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using xytools;

namespace schulman
{
    /// <summary>
    /// Interaktionslogik für LessonTypeSettings.xaml
    /// </summary>
    public partial class LessonTypeSettings : Window
    {
        public static byte red = 0;
        public static byte green = 0;
        public static byte blue = 255;
        public static byte alpha = 255;
        public static string hexRed = "00";
        public static string hexBlue = "FF";
        public static string hexGreen = "00";
        public static string hexFull = "0000FF";
        public LessonTypeSettings()
        {
            InitializeComponent();
            SchulMan.lessonTypeSet = this;
        }

        public void updateHex()
        {
            hexRed = red.ToString("X2");
            hexGreen = green.ToString("X2");
            hexBlue = blue.ToString("X2");
            hexFull = hexRed + hexGreen + hexBlue;
            txtHex.Text = hexFull;

        }

        public void updateColor()
        {
            rctCol.Fill = new SolidColorBrush(Color.FromArgb(alpha, red, green, blue));
        }

        private void sliderGreen_ValueChanged_1(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            try
            {
                D.W("Green Value changed: " + e.NewValue);
                green = (byte)e.NewValue;
                updateColor();
                updateHex();
            }
            catch (Exception)
            {
                MessageBox.Show("Inkorrekter Wert für grün!");
            }
        }

        private void sliderRed_ValueChanged_1(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            try
            {
                D.W("Red Value changed: " + e.NewValue);
                red = (byte)e.NewValue;
                updateColor();
                updateHex();
            }
            catch (Exception)
            {
                MessageBox.Show("Inkorrekter Wert für rot!");
            }
        }

        private void sliderBlue_ValueChanged_1(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            try
            {
                D.W("Blue Value changed: " + e.NewValue);
                blue = (byte)e.NewValue;
                updateColor();
                updateHex();
            }
            catch (Exception)
            {
                MessageBox.Show("Inkorrekter Wert für blau!");
            }
        }

        private void sliderAlpha_ValueChanged_1(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            try
            {
                D.W("Alpha Value changed: " + e.NewValue);
                alpha = (byte)e.NewValue;
                updateColor();
            }
            catch (Exception)
            {
                MessageBox.Show("Inkorrekter Wert für Alphakanal (Transparenz)!");
            }
        }

        private void Window_ContentRendered_1(object sender, EventArgs e)
        {
            xytools.AeroTools.RefreshAero(this);
        }

        private void Window_GotFocus_1(object sender, RoutedEventArgs e)
        {
            xytools.AeroTools.RefreshAero(this);
        }

        private void Window_LayoutUpdated_1(object sender, EventArgs e)
        {
            xytools.AeroTools.RefreshAero(this);
        }

        private void Window_LocationChanged_1(object sender, EventArgs e)
        {
            xytools.AeroTools.RefreshAero(this);
        }

        private void Window_SizeChanged_1(object sender, SizeChangedEventArgs e)
        {
            xytools.AeroTools.RefreshAero(this);
        }

        private void cboxPresets_Loaded(object sender, RoutedEventArgs e)
        {
            Type colorType = typeof(System.Drawing.Color);
            PropertyInfo[] propInfos = colorType.GetProperties(BindingFlags.Static | BindingFlags.DeclaredOnly | BindingFlags.Public);
            foreach (PropertyInfo item in propInfos)
            {
                ComboBoxItem cbi = new ComboBoxItem();
                cbi.Content = item.ToString().Replace("System.Drawing.Color ","");
                cboxPresets.Items.Add(cbi);
            }
        }

        private void cboxPresets_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try{
                if (cboxPresets.SelectedItem == null || ((ComboBoxItem)cboxPresets.SelectedItem).Content == null) return;
                Color col = (Color)ColorConverter.ConvertFromString(((ComboBoxItem)cboxPresets.SelectedItem).Content.ToString());
                //MessageBox.Show(((ComboBoxItem)cboxPresets.SelectedItem).Content.ToString());
                //D.W("col="+col.ToString());
                redVal.Text = col.R.ToString(); greenVal.Text = col.G.ToString(); blueVal.Text = col.B.ToString();
                aVal.Text = col.A.ToString();
                red = col.R; green = col.G; blue = col.B; alpha = col.A;
                updateColor(); updateHex();
            }
            catch (Exception ex) { MessageBox.Show("Fehler beim Setzen der Farbe :("); D.W("Exception in cboxPresets_SelectionChanged: " + ex.Message + "\nStackTrace:" + ex.StackTrace); }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            //SAVE NYI
            this.Close();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            //ABORT NYI
            this.Close();
        }
    }
}
