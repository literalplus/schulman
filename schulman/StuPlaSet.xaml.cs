﻿/*************************************************************\\
 * File: StuPlaSet.xaml.cs
 * Schulman v(see SchulMan.Version in File App.xaml.cs)
 * by _xxyy(Philipp N.)
 * xxyy98+schulman@gmail.com
 * http://xxyy.bplaced.net/
 * 
 * For full source code and updates, please check
 * <http://bit.ly/_schulman>
 *   
 * This program is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU General
 *   Public License as published by the Free Software
 *   Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *   
 * This program is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied
 *   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *   PURPOSE.  See the GNU General Public License for more
 *   details.
 *   
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
\\*************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using xytools;
using System.IO;

namespace schulman
{
    /// <summary>
    /// Interaktionslogik für Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        public TextBox[][] boxes;
        public Window1()
        {
            InitializeComponent();
            boxes = new TextBox[][]{
                new TextBox[8]{ tbMo1,tbMo2,tbMo3,tbMo4,tbMo5,tbMo6,tbMo7,tbMo8 }, //like a monday
                new TextBox[8]{ tbDi1,tbDi2,tbDi3,tbDi4,tbDi5,tbDi6,tbDi7,tbDi8 }, //like a tuesday
                new TextBox[8]{ tbMi1,tbMi2,tbMi3,tbMi4,tbMi5,tbMi6,tbMi7,tbMi8 }, //like a wednesday
                new TextBox[8]{ tbDo1,tbDo2,tbDo3,tbDo4,tbDo5,tbDo6,tbDo7,tbDo8 }, //like a thursday
                new TextBox[8]{ tbFr1,tbFr2,tbFr3,tbFr4,tbFr5,tbFr6,tbFr7,tbFr8 } //like a friday
            };
            SchulMan.stuplaset = this;
            LoadLessonsToTextBoxes();
        }
        public void LoadLessonsToTextBoxes()
        {
            try
            {
                Lesson[][] lessons = Lessons.LoadLessonsFromConfig();
                int j = 0;
                foreach (Lesson[] day in lessons)
                {
                    int i =0;
                    foreach (Lesson lesson in day)
                    {
                        D.W(j+lesson.Key + i, "j,Lesson.Key,i");
                        boxes[j][i].Text = lesson.Key;
                        i++;
                    }
                    j++;
                }
            }
            catch (FileNotFoundException)
            {
                D.W("Config file doesn't exist; Leaving TextBoxes empty.");
            }
        }
        public void DoAero()
        {
            xytools.AeroTools.RefreshAero(this, -1, (int)spActions.ActualHeight);
        }

        private void spActions_GotFocus(object sender, RoutedEventArgs e)
        {
            this.DoAero();
        }

        private void spActions_Initialized(object sender, EventArgs e)
        {
            this.DoAero();
        }

        private void spActions_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            this.DoAero();
        }

        private void spActions_LayoutUpdated(object sender, EventArgs e)
        {
            this.DoAero();
        }

        private void spActions_Loaded(object sender, RoutedEventArgs e)
        {
            this.DoAero();
        }

        private void spActions_ManipulationStarted(object sender, ManipulationStartedEventArgs e)
        {
            this.DoAero();
        }

        private void spActions_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            this.DoAero();
        }

        private void spActions_ManipulationCompleted(object sender, ManipulationCompletedEventArgs e)
        {
            this.DoAero();
        }

        private void Window_Activated_1(object sender, EventArgs e)
        {
            this.DoAero();
        }

        private void Window_ContentRendered_1(object sender, EventArgs e)
        {
            this.DoAero();
        }

        private void Window_GotFocus_1(object sender, RoutedEventArgs e)
        {
            this.DoAero();
        }

        private void Window_LocationChanged_1(object sender, EventArgs e)
        {
            this.DoAero();
        }

        private void Window_LayoutUpdated_1(object sender, EventArgs e)
        {
            this.DoAero();
        }

        private void Window_SizeChanged_1(object sender, SizeChangedEventArgs e)
        {
            this.DoAero();
        }

        private void btnAbort_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                pbBottomBar.Visibility = Visibility.Visible;
                Lessons.SaveLessonsToConfig(boxes, pbBottomBar);

                SchulMan.mainWin.ReloadStuPla();
            }
            catch (Exception ex)
            {
                SchulMan.devenv.tbDev.Text = ex.Message;
                MessageBox.Show("Die Änderungen konnten NICHT gespeichert werden!\n Weitere Informationen:" + ex.Message);
                D.W(ex.StackTrace);
            }
            this.Close();
        }
    }
}
