﻿/*************************************************************\\
 * File: devenv.xaml.cs
 * Schulman v(see SchulMan.Version in File App.xaml.cs)
 * by _xxyy(Philipp N.)
 * xxyy98+schulman@gmail.com
 * http://xxyy.bplaced.net/
 * 
 * For full source code and updates, please check
 * <http://bit.ly/_schulman>
 *   
 * This program is free software: you can redistribute it
 *   and/or modify it under the terms of the GNU General
 *   Public License as published by the Free Software
 *   Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *   
 * This program is distributed in the hope that it will be
 *   useful, but WITHOUT ANY WARRANTY; without even the implied
 *   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *   PURPOSE.  See the GNU General Public License for more
 *   details.
 *   
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
\\*************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace schulman
{
    /// <summary>
    /// Interaktionslogik für devenv.xaml
    /// </summary>
    public partial class devenv : Window
    {
        public devenv()
        {
            InitializeComponent();
            SchulMan.devenv = this;
        }

        private void btnMainWin_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mainw = new MainWindow();
            mainw.Show();
        }

        private void btnStuplaSet_Click(object sender, RoutedEventArgs e)
        {
            LessonTypeSettings lts=new LessonTypeSettings();
            lts.Show();
        }

        private void btnSetPB_Click(object sender, RoutedEventArgs e)
        {
            startscreen sc = new startscreen(new CancellationToken());
            sc.ShowDialog();

        }
    }
}
