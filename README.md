# Schulman v0.1a
This application adds incredible things (like a clock!).
******
Schulman v.01a (GERMAN)
=======================
Diese Application hat unglaubliche Features, zum Beispiel eine *Uhr* und einen... 

...*Stundenplan!*
Stundenplan
-----------
Er ist sogar konfigurierbar! Aktuell werden nur wenige vorgegebene Stundentypen unterstützt, aber es können auch unbekannte benutzt werden, nur eben ohne Hintergundfarbe. 
*[Pro-Tipp]* Mit der Tastenkombination 'Windows \+ Umschalt \+ D'wird ein Debugfenster geöffnet. Der 2. Button von oben "Stundenplaneinstellungen" führt zu einem Fenster, in dem man die Stunden des Stundenplans ändern kann.

_Note:_ *This application is nowhere near to complete and it's code is nowhere near to good - Please understand that this project is not actually maintained a lot any more.*
